variable "gcp_project_id" {
  type        = string
  description = "The GCP project id"
}

resource "google_service_account" "gkedeploy_sa" {
  account_id   = "gkedeploy-sa"
  display_name = "Service Account"
  project      = var.gcp_project_id
}
